export interface Candlestick {
  open: number,
  close: number,
  high: number,
  low: number,
  date: Date
}
