import { Component, ViewChild } from '@angular/core';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexTitleSubtitle,
  ApexYAxis
} from "ng-apexcharts";
import { Candlestick } from './models/candlestick';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis;
  title: ApexTitleSubtitle;
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild("chart") chart: ChartComponent;
  chartOptions: ChartOptions = {
    series: [{
      name: "candle",
      data:[
      ]
    }],
    chart: {
      height: "900hv",
      type: "candlestick"
    },
    title: {
      text: "Broker"
    },
    xaxis: {
      type: 'datetime'
    },
    yaxis: {
      tooltip:{
        enabled: true
      }
    }
  };
  constructor () {
    const ws = new WebSocket('ws://localhost:3000');
    ws.onmessage = (event) => {
      const cs: Candlestick = JSON.parse(event.data) as Candlestick;
      let data: any = this.chartOptions.series[0].data;
      data.push([cs.date,[cs.open,cs.high,cs.low,cs.close]])
      this.chart.updateSeries([{
        name: "candle",
        data: data
      }])
    }
  }
  get series():ApexAxisChartSeries{
    return this.chartOptions.series;
  }
}
